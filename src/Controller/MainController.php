<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use App\Form\ImportFileFormType;
use App\Service\FileImporter;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/main", name="app_main")
     */
    public function index(Request $request, EntityManagerInterface $entityManager): Response
    {
        if (!$this->getUser()) {
            return $this->redirectToRoute('app_login');
        }

        return $this->render('main/index.html.twig', [
            'users' => $entityManager->getRepository(User::class)->findAll(),
            'importForm' => $this
                ->createForm(ImportFileFormType::class)
                ->createView(),
        ]);
    }

    /**
     * @Route("/import", name="app_import")
     */
    public function import(Request $request, FileImporter $fileImporter): Response
    {
        $file = $request->files->get('import_file_form')['importFile'] ?? null;
        if (!empty($file)) {
            $fileImporter->import($file);
        } else {
            $this->addFlash('danger', 'Import file error');
        }

        return $this->redirectToRoute('app_main');
    }
}
