<?php

declare(strict_types=1);

namespace App\Controller;

use App\Exception\ChangePasswordException;
use App\Form\ChangePasswordFormType;
use App\Service\UserWriter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('app_main');
        }

        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error,
        ]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout(): void
    {
    }

    /**
     * @Route("/password-change", name="app_password_change")
     */
    public function passwordChange(
        Request $request,
        UserWriter $userWriter
    ): Response {
        $form = $this->createForm(ChangePasswordFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            try {
                $userWriter->changePassword(
                    $form->get('email')->getData(),
                    $form->get('oldPassword')->getData(),
                    $form->get('newPassword')->getData(),
                );
                $this->addFlash('success', 'Password successfully changed, please log in');

                return $this->redirectToRoute('app_login');
            } catch (ChangePasswordException $exception) {
                $this->addFlash('danger', $exception->getMessage());
            }
        }

        return $this->render('security/changePassword.html.twig', [
            'changePasswordForm' => $form->createView(),
            'error' => $form->getErrors(true)->getChildren(),
        ]);
    }
}
