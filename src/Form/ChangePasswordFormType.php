<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ChangePasswordFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', TextType::class, ['attr' => ['class' => 'form-control', 'placeholder' => 'Email']])
            ->add('oldPassword', PasswordType::class, [
                'attr' => ['required' => 'required', 'placeholder' => 'Old password', 'class' => 'form-control'],
                'constraints' => [
                    new NotBlank(),
                    new Length(['min' => 6, 'max' => 30]),
                ]
            ])
            ->add('newPassword', PasswordType::class, [
                'label' => 'New password',
                'attr' => ['required' => 'required', 'placeholder' => 'New password', 'class' => 'form-control'],
                'constraints' => [
                    new NotBlank(),
                    new Length(['min' => 6, 'max' => 30]),
                ]
            ])
        ;
    }
}
