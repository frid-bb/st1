<?php

declare(strict_types=1);

namespace App\Service;

use App\Exception\ChangePasswordException;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserWriter
{
    private EntityManagerInterface $entityManager;
    private UserRepository $userRepository;
    private UserPasswordHasherInterface $userPasswordHasher;
    private FlashBagInterface $flashBag;

    public function __construct(
        EntityManagerInterface $entityManager,
        UserRepository $userRepository,
        UserPasswordHasherInterface $userPasswordHasher,
        FlashBagInterface $flashBag
    ) {
        $this->entityManager = $entityManager;
        $this->userRepository = $userRepository;
        $this->userPasswordHasher = $userPasswordHasher;
        $this->flashBag = $flashBag;
    }

    /**
     * @throws ChangePasswordException
     */
    public function changePassword(string $email, string $oldPassword, string $newPassword): void
    {
        $user = $this->userRepository->findOneBy(['email' => $email]);
        if (empty($user)) {
            throw new ChangePasswordException('User doesnt exist');
        } else if ($newPassword === $oldPassword) {
            throw new ChangePasswordException('Both passwords are equal');
        } else if ($this->userPasswordHasher->isPasswordValid($user, $oldPassword)) {
            $user->setPassword(
                $this->userPasswordHasher->hashPassword(
                    $user,
                    $newPassword
                )
            );
            $user->setLastLoginAt(new \DateTime());
            $this->entityManager->persist($user);
            $this->entityManager->flush();
        } else {
            throw new ChangePasswordException('Old password invalid');
        }
    }
}