<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class FileImporter
{
    private EntityManagerInterface $entityManager;
    private UserPasswordHasherInterface $userPasswordHasher;
    private FlashBagInterface $flashBag;
    private MailerInterface $mailer;

    public function __construct(
        EntityManagerInterface $entityManager,
        UserPasswordHasherInterface $userPasswordHasher,
        FlashBagInterface $flashBag,
        MailerInterface $mailer
    ) {
        $this->entityManager = $entityManager;
        $this->userPasswordHasher = $userPasswordHasher;
        $this->flashBag = $flashBag;
        $this->mailer = $mailer;
    }

    public function import(UploadedFile $file): void
    {
        try {
            $emails = [];
            $spreadsheet = IOFactory::load($file->getRealPath());
            $spreadsheetData = $spreadsheet->getActiveSheet()->toArray();

            foreach ($spreadsheetData as $key => $row) {
                if ($key > 0) {
                    $password = str_replace('=', '', base64_encode(random_bytes(5)));
                    $user = new User();
                    $user->setName($row[0]);
                    $user->setSurname($row[1]);
                    $user->setEmail($row[2]);
                    $user->setBirthDate(new \DateTime($row[3]));
                    $user->setPassword($this->userPasswordHasher->hashPassword($user, $password));
                    $user->setCreatedAt(new \DateTime());
                    $this->entityManager->persist($user);
                    $emails[$row[2]] = $password;
                }
            }

            $this->entityManager->flush();
            $this->flashBag->add('success', 'Data imported');

            foreach ($emails as $email => $password) {
                $this->mailer->send((new Email())
                    ->from('test@test.com')
                    ->to($email)
                    ->subject('Account created')
                    ->text('Password: ' . $password)
                );
            }

        } catch (\Exception $exception) {
            $this->flashBag->add('danger', $exception->getMessage());
        }
    }
}